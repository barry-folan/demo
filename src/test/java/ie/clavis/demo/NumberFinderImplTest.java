package ie.clavis.demo;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class NumberFinderImplTest {

    public final String filename = "custom_number_entities.json";
    private final int presentValue = 45;
    private final int missingValue = 9999;
    private List<CustomNumberEntity> cnes;

    @Autowired
    private NumberFinderImpl numberFinderImpl;

    @Before
    public void setup() {
        cnes = numberFinderImpl.readFromFile(filename);
    }

    @Test
    public void containsTrue() {
        assertTrue(numberFinderImpl.contains(presentValue, cnes));
    }

    @Test
    public void containsFalse() {
        assertFalse(numberFinderImpl.contains(missingValue, cnes));
    }

    @Test
    public void readFromFile() {
        assertFalse(cnes.isEmpty());
    }
}
