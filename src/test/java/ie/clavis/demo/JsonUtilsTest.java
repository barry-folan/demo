package ie.clavis.demo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest
public class JsonUtilsTest {

    public final String filename = "custom_number_entities.json";

    @Autowired
    JsonUtils jsonUtils;

    @Test
    public void loadJson() {
        int cores = Runtime.getRuntime().availableProcessors();
        String json = jsonUtils.loadJson(filename);
        Assert.assertNotNull(json);
        Assert.assertFalse(json.isEmpty());
    }

    @Test
    public void parseJson() {
        String json = jsonUtils.loadJson(filename);
        List<CustomNumberEntity> cnes = jsonUtils.parseJson(json);
        Assert.assertFalse(cnes.isEmpty());
    }
}
