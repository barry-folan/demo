package ie.clavis.demo;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

@Component
public class JsonUtils {

    public List<CustomNumberEntity> parseJson(String json) {
        Type listType = new TypeToken<ArrayList<CustomNumberEntity>>() {
        }.getType();
        return new Gson().fromJson(json, listType);
    }

    public String loadJson(String filename) {
        StringBuilder jsonSb = new StringBuilder("");
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                jsonSb.append(line).append("\n");
            }
            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonSb.toString();
    }
}
