package ie.clavis.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.*;

/**
 * Class used to check if a value exists in a Json list loaded from file.
 * The class uses multi-threading to check the list as fast as possible.
 * <p>
 * The List<CustomNumberEntity> is searched by several threads (Futures) in parallel.
 * Each thread searches a different segment of the list.
 * The number of threads used matches the number of CPU processor cores available.
 * <p>
 * (It would be inefficient to sort the list as it is only going to be searched once)
 */
@Component
public class NumberFinderImpl implements NumberFinder {

    private final ExecutorService executorService;
    private final int numChildThreads;
    private final ArrayList<Future<Boolean>> futures;
    @Autowired
    private JsonUtils jsonUtils;

    public NumberFinderImpl() {
        // the number of processor cores available for use
        int cores = Runtime.getRuntime().availableProcessors();
        this.numChildThreads = cores - 1;
        this.executorService = Executors.newFixedThreadPool(numChildThreads);
        this.futures = new ArrayList(numChildThreads);
    }

    /**
     * Check if valueToFind exists in the list.
     */
    @Override
    public boolean contains(int valueToFind, List<CustomNumberEntity> list) {

        // Divide the list up into segments. 1 segment per cpu processor core.
        int startIndex = 0;
        int segmentSize = list.size() / numChildThreads;
        int endIndex = segmentSize - 1;

        // Start a NumberFinderTask per thread, each with its own specific list start and end indexes
        for (int i = 0; i < numChildThreads; i++) {
            NumberFinderTask numberFinderTask = new NumberFinderTask(valueToFind, list, startIndex, endIndex);
            futures.add(executorService.submit(numberFinderTask));
            startIndex = endIndex + 1;
            endIndex = endIndex + segmentSize;
        }

        // loop through futuresIter until all tasks complete or valueToFind is found
        // any completed task that returns false (valueToFind not present) can be removed so that it is no longer checked.
        ListIterator<Future<Boolean>> futuresIter = futures.listIterator();
        while (futuresIter.hasNext() || futuresIter.hasPrevious()) {
            Future<Boolean> f = futuresIter.hasNext() ? futuresIter.next() : futuresIter.previous();
            // if the task has completed
            if (f.isDone()) {
                try {
                    if (f.get()) {
                        // The valueToFind was found in the segment of the list covered by this task.
                        cancelAllFutures();
                        return true;
                    } else {
                        // The valueToFind is not present in this segment of the list. Remove the future from the iterator.
                        futuresIter.remove();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        // all tasks completed without finding the valueToFind
        return false;
    }

    /**
     * Read the JSON file into memory
     */
    @Override
    public List<CustomNumberEntity> readFromFile(String filePath) {
        String json = jsonUtils.loadJson(filePath);
        return jsonUtils.parseJson(json);
    }

    /**
     * cancel all unfinished Futures
     */
    private void cancelAllFutures() {
        // implement
    }
}

/**
 * A task that checks if a value exists in a list between the given indexes
 */
class NumberFinderTask implements Callable<Boolean> {

    private final FastestComparator fastestComparator;
    private final int valueToFind;
    private final List<CustomNumberEntity> list;
    private final int startIndex;
    private final int endIndex;


    public NumberFinderTask(int valueToFind, List<CustomNumberEntity> list, int startIndex, int endIndex) {
        fastestComparator = new FastestComparator();
        this.valueToFind = valueToFind;
        this.list = list;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public Boolean call() throws Exception {
        return contains();
    }

    /**
     * Check if valueToFind exists in list between the indexes
     *
     * @return true if valueToFind exists in list between startIndex and endIndex
     */
    private boolean contains() {
        for (int i = startIndex; i <= endIndex; i++) {
            CustomNumberEntity cne = list.get(i);
            try {
                if (fastestComparator.compare(valueToFind, cne) == 0) {
                    return true;
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        return false;
    }
}